package com.retroroots.composenavigationwithsafeargtest.ui

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.retroroots.composenavigationwithsafeargtest.ui.nav.Screen

@Composable
fun Screen1(navHostController: NavHostController)
{
    Column(modifier = Modifier.fillMaxSize())
    {
        Text(text = "Screen1")

        Button(
            onClick = {
                navHostController.navigate(route = Screen.Screen2.name)
            },
        ) {
            Text(text = "Next")
        }
    }
}

@Composable
fun Screen2(navHostController: NavHostController)
{
    Column(modifier = Modifier.fillMaxSize())
    {
        Text(text = "Screen2")

        Button(onClick = {
            navHostController.popBackStack()
        }) {
            Text(text = "Back")
        }
    }
}


@Preview
@Composable
fun S1Preview()
{
    Screen1(rememberNavController())
}

@Preview
@Composable
fun S2Preview()
{
    Screen2(rememberNavController())
}