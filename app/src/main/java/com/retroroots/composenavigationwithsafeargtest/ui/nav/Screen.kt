package com.retroroots.composenavigationwithsafeargtest.ui.nav

enum class ScreenName
{
    S1,
    S2
}
sealed class Screen(val name: String)
{
    data object Screen1 : Screen(ScreenName.S1.name)

    data object Screen2 : Screen(ScreenName.S2.name)
}