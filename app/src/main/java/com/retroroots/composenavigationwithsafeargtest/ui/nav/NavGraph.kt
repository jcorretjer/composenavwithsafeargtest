package com.retroroots.composenavigationwithsafeargtest.ui.nav

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.retroroots.composenavigationwithsafeargtest.ui.Screen1
import com.retroroots.composenavigationwithsafeargtest.ui.Screen2

@Composable
fun ConfigNavGraph(navHostController: NavHostController)
{
    NavHost(navController = navHostController, startDestination = Screen.Screen1.name)
    {
        composable(
            Screen.Screen1.name
        ){
            Screen1(navHostController)
        }

        composable(
            Screen.Screen2.name
        ){
            Screen2(navHostController)
        }
    }
}