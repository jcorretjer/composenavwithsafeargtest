package com.retroroots.composenavigationwithsafeargtest.ui

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.retroroots.composenavigationwithsafeargtest.ui.nav.ConfigNavGraph

class MainActivity : ComponentActivity()
{
    private lateinit var navHostController: NavHostController

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        setContent {
            MainUI()
            }
        }

    @Composable
    fun MainUI()
    {
        MaterialTheme {
            navHostController = rememberNavController()

            ConfigNavGraph(navHostController = navHostController)
        }
    }

    @Preview(showBackground = true)
    @Composable
    fun MainUIPreview()
    {
        MainUI()
    }
}